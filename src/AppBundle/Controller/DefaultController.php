<?php

namespace AppBundle\Controller;

use Doctrine\ORM\Query\ResultSetMapping;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $stmt = $em->getConnection()->prepare('SELECT * FROM sample_table');
        $stmt->execute();
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'result' => $stmt->fetchAll(),
        ]);
    }
}
